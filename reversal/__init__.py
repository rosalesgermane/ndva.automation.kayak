from typing import List


def check_balance(text: str):
    """
    Checks whether a string has the same number of opening and closing parentheses
    :param text:
    :return: true when balanced, false when unbalanced
    """
    return text.count("(") == text.count(")")


def reverse_nested_texts(raw_text: str) -> str:
    """
    function that reverses characters in (possibly nested) parentheses in the input string.
    Input strings will always be well-formed with matching ()s.
    :param raw_text:
    :return str: reversed parentheses text
    :raises ValueError: when the input raw_text is bigger than 50 characters or if not balanced
    """
    if len(raw_text) > 50 or not check_balance(raw_text):
        raise ValueError()
    stack = [[]]  # accumulate letters in stack
    for letter in raw_text:
        if letter == '(':
            stack.append([])  # start a new level
        elif letter == ')':
            reversed_last_level = stack.pop()[::-1]  # pop last level and reverse
            stack[-1].extend(reversed_last_level)  # add to last item of the array
        else:
            stack[-1].append(letter)  # add to current

    return ''.join(stack[0])



