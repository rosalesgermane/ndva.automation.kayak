# Assessment QA engineer

## 1: Reversal of Nested parentheses

The first script uses pytest for unit testing the required function. 

### Files involved

```
reversal/*
test/*
```

### Running
From the project folder, follow the next steps:

```
pipenv install
pipenv shell
pytest test/test_reversal.py
```

## 2. Automation on KAYAK

This automation runs on Behave BDD framework and Selenium along with other helper libraries included in the pipfile. Testing scenarios and assumptions are documented properly in the feature files included. The required funtionality is far from complete because of the time, but I think it illustrates most of the required knowledge.

### Files involved

```
src/*
```

### Running

To prepare your local machine install the chromedriver:

```
choco install chromedriver
```

Then prepare the python environment, if you did this for step 1 and your are already into de environment shell please skip
```
cd %PROJECT_DIR%/
pipenv intall
pipenv shell
```

Now run it using the helper `main.py`
```
cd src
python3 main.py ./kayak/features
```

OR use directly behave
```
cd src
behave ./kayak/features
```
