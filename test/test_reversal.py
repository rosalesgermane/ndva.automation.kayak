from reversal import reverse_nested_texts


def test_reverse_nested_texts_empty_string():
    """
    Multiple nodes in a single level
    """
    assert reverse_nested_texts("") == ""


def test_reverse_nested_texts_single_level():
    """
    Multiple nodes in a single level
    """
    assert reverse_nested_texts("ab(cdefg)h(ij)kl(mno)(pq)") == "abgfedchjiklonmqp"


def test_reverse_nested_texts_two_levels():
    """
    Multiple nodes in the second  level
    """
    assert reverse_nested_texts("(abcd(efgh)ijkl(mno)p)q") == "pmnolkjiefghdcbaq"


def test_reverse_nested_texts_three_levels():
    """
    Multiple nodes in the third level
    """
    reversed_text = reverse_nested_texts("a(b(c(defgh)i(jk)lm)(nop)q)r")
    assert reversed_text == "aqnopchgfedikjlmbr"


def test_reverse_nested_texts_empty_parentheses():
    """
    a text with multiple empty nesting parentheses
    """
    reversed_text = reverse_nested_texts("a(b(c(de()fgh)ij(())klm)(n((()))op)q)r")
    assert reversed_text == "aqnopchgfedijklmbr"


def test_reverse_nested_texts_consecutive_nested_parenthesis():
    """
    Consecutive nesting
    """
    reversed_text = reverse_nested_texts("((((abcdef))))(((gh)ij)k)")
    assert reversed_text == "abcdefkhgij"


def test_reverse_nested_text_big_text():
    """
    50 len input text
    """
    reversed_text = reverse_nested_texts("a(b(c(d(df(5(67)8)9)h)i2(3(4)6)7)(no(3()458)p)q)rs")
    assert reversed_text == "aqno8543pchdf86759di26437brs"

