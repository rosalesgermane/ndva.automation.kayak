from behave import fixture, use_fixture
from splinter import Browser

from pom import HomePage


@fixture(name='fixture.browser.chrome')
def browser_chrome(context):
    # TODO: send capabilities from configuration
    browser = Browser('chrome')
    browser.driver.implicitly_wait(20)
    context.browser = browser
    yield context.browser
    # cleanup
    context.browser.quit()


@fixture
def base_url(context):
    # TODO: inject this from configuration
    context.base_url = "https://www.kayak.com.co"
    yield context.base_url


def before_tag(context, tag: str):
    if tag.startswith("fixture.browser"):
        use_fixture(base_url, context)
        if tag == "fixture.browser.chrome":
            use_fixture(browser_chrome, context)
        context.home_page = HomePage(context.browser, context.base_url)


def before_scenario(context, scenario):
    use_fixture(base_url, context)
    if hasattr(context, 'browser'):
        context.browser.cookies.delete()

# def after_scenario(context, scenario):
#     if 'no_ui' in scenario.tags or 'no_ui' in context.feature.tags:
#         return
#     else:
#         context.browser.stop()
