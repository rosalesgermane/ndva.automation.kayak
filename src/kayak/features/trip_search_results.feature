#Feature: Travelers
#
#  Background:
#    Given There are trips available from <OriginAirportCode> to <DestinyAirportCode> in Kayak database
#
#  Scenario Outline: As a traveler I should be able to change the departure and return dates
#    Given I searched <OriginAirportCode> to <DestinyAirportCode>
#    And There is at least one trip in my search result list
#    When I select a new departure <DepartureDate> and Return <ReturnDate> dates
#    And I start my search
#    Then The system should display the information related to each trip and it should match my search criteria: stopovers, estimated durations, price, time of departure, estimated time of arrivals, airline
#
#    Examples:
#      | OriginAirportCode | DestinyAirportCode | DepartureDate | ReturnDate |
#      | MDE               | SFO                | 2022-06-15    | 2022-06-20 |
#
#  Scenario Outline: As a traveler I should be able to open the lowest price trip
#    Given I searched <OriginAirportCode> to <DestinyAirportCode>
#    And There is at least one trip in my search result list
#    And The first result record its a sponsored result displayed
#    When I select ordering by lower price
#    And I select the first trip after the sponsored result
#    Then The system should display TODO:
#    Examples:
#      | OriginAirportCode | DestinyAirportCode |