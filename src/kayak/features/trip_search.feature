Feature: Travelers are able to search for trips
  As a traveler I should be able to search for trips from an origin to a destiny and select the ones that better suits my needs

  Background:
    Given The traveler is on the Home page
    And The page selected the user origin location by default based on the user location
    And The page selects trip type "Ida y vuelta" by default
    And The page selects trip class "Económica" by default
    # And The page selected a default departure date 30 days in the future
    # And The page selected a default return date of 37 days in the future

  @fixture.browser.chrome
  Scenario Outline: As a traveler I should be able to search for trips from origin to destiny
    Given There are Airports in the desired Origin <OriginLocationName>
    # And There are trips available from <OriginAirportCode> to <DestinyAirportCode> in Kayak database
    When I clean origin's default value
    And I search for Origin <OriginLocationName> and select <OriginAirportCode>
    And I search for Destination <DestinationLocationName> and selects <DestinationAirportCode>
    And I select <Adults> and <Child> travelers
    And I select a new departure <DepartureDate> and Return <ReturnDate> dates
    And I start my search
    Then The system should display at least one trip from <OriginAirportCode> to <DestinyAirportCode> in <DepartureDate>
#    And The system should display the parameters: OriginAirportCode, DestinyAirportCode, DepartureDate, ReturnDate
#    And The system should display the best result suggestion by default
#    And The system should display the information related to each trip and it should match my search criteria: stopovers, estimated durations, price, time of departure, estimated time of arrivals, airline
    Examples: Dates here should be in the timezone of the machine running the browser
      | OriginLocationName | OriginAirportCode | DestinationLocationName | DestinationAirportCode | Adults | Child | DepartureDate | ReturnDate |
      | Medellín           | EOH               | San Francisco           | SFO                    | 2      | 1     | 2022-04-17    | 2022-04-25 |

