from __future__ import annotations
from time import sleep
from typing import List
from pypom import Page, Region
from splinter.driver.webdriver import WebDriverElement
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from datetime import date


class HomePage(Page):
    URL_TEMPLATE = '/'

    _title = ('css', 'h2.title')
    _origins = ('css', ".zEiP-origin")
    _destinations = ('css', ".zEiP-destination")
    _search_button = ('css', 'button[aria-label="Buscar"]')
    _date_control = ('css', '.cQtq-input:nth-of-type({0})')

    @property
    def title(self) -> WebDriverElement:
        return self.find_element(*self._title)

    @property
    def destinations(self) -> SelectedLocations:
        return self.SelectedLocations(self, root=self.find_element(*self._destinations))

    @property
    def origins(self) -> SelectedLocations:
        return self.SelectedLocations(self, root=self.find_element(*self._origins))

    @property
    def search_button(self) -> WebDriverElement:
        return self.find_element(*self._search_button)

    @property
    def loaded(self):
        return self.is_element_displayed(*self._search_button)

    @property
    def trip_type(self) -> TripType:
        return self.TripType(self)

    @property
    def trip_class(self) -> TripClass:
        return self.TripClass(self)

    @property
    def trip_travelers(self) -> TripTravelers:
        return self.TripTravelers(self)

    @property
    def trip_travelers_dropdown(self) -> TripTravelersDropDown:
        return self.TripTravelersDropDown(self)

    @property
    def location_dialog(self) -> LocationDialog:
        return self.LocationDialog(self)

    @property
    def departing_date_control(self) -> DateControl:
        return self.DateControl(self,
                                root=self.find_element(self._date_control[0], self._date_control[1].format(1)))

    @property
    def returning_date_control(self) -> DateControl:
        return self.DateControl(self,
                                root=self.find_element(self._date_control[0], self._date_control[1].format(3)))

    class SelectedLocations(Region):
        _list_items = ('css', '[role="listitem"]')
        _text_box = ('css', '[role="textbox"]')

        @property
        def list_items(self) -> List[SelectionListItem]:
            if self.is_element_displayed(*self._list_items):
                sel_items = [self.SelectionListItem(self, root=el) for el in self.find_elements(*self._list_items)]
                return sel_items
            else:
                return []

        @property
        def text_box(self) -> WebDriverElement:
            return self.find_element(*self._text_box)

        class SelectionListItem(Region):
            _remover = ('css', "[aria-label='Eliminar']")
            _selected_value = ('css', '.vvTc-item-value')

            @property
            def remover(self) -> WebDriverElement:
                return self.find_element(*self._remover)

            @property
            def selected_value(self) -> WebDriverElement:
                return self.find_element(*self._selected_value)

    class LocationDialog(Region):
        _root_locator = ('css', '[role="dialog"].c8GSD')
        _textbox = ('css', 'input.k_my-input')
        _search_results = ('css', '[role="tablist"] li[role="tab"]')
        _find_search_result_by_airport_code = (
            'xpath',
            """//*[contains(@class, 'JyN0-airportCode')]/text()[contains(.,"{0}")]/../../..""")

        @property
        def loaded(self):
            # displayed = self.is_element_displayed(*self._textbox)
            elem = WebDriverWait(self.driver.driver, 10).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, self._textbox[1]))
            )
            clickable = False if not elem else True
            return clickable

        def wait_stale(self) -> WebDriverElement:
            elem: WebDriverElement = None
            try:
                elem = WebDriverWait(self.driver.driver, 5).until(
                    EC.element_to_be_clickable(self.root)
                )
            finally:
                return elem

        @property
        def text_box(self) -> WebDriverElement:
            return self.find_element(*self._textbox)

        @property
        def search_results(self) -> List[LocationSearchResult]:
            if self.driver.is_element_present_by_css(self._search_results[1], wait_time=30):
                return [
                    self.LocationSearchResult(self, root=el) for el in self.find_elements(*self._search_results)
                ]
            else:
                return []

        def find_search_result_by_airport_code(self, airport_code: str) -> LocationSearchResult:
            """
            Search and returns the required search result by airport code
            :return: LocationSearchResult
            """
            elem = self.find_element(self._find_search_result_by_airport_code[0],
                                     self._find_search_result_by_airport_code[1].format(airport_code))
            return self.LocationSearchResult(self, root=elem)

        class LocationSearchResult(Region):
            _airport_code = ('css', '.JyN0-airportCode')
            _checkbox = ('css', '.PVIO-input-wrapper')

            @property
            def airport_code(self) -> WebDriverElement:
                return self.find_element(*self._airport_code)

            @property
            def checkbox(self) -> WebDriverElement:
                return self.find_element(*self._checkbox)

    class TripType(Region):
        _root_locator = ('xpath', '(//*[@class="zcIg"]//*[@role="button"])[1]')
        _selected_value = ('css', 'span:nth-child(1)')
        _expand_control = ('css', 'span:nth-child(2)')

        @property
        def selected_value(self) -> str:
            elem = self.find_element(*self._selected_value)
            return elem.text

    class TripClass(Region):
        _root_locator = ('xpath', '(//*[@class="zcIg"]//*[@role="button"])[3]')
        _selected_value = ('css', 'span:nth-child(1)')
        _expand_control = ('css', 'span:nth-child(2)')

        @property
        def selected_value(self) -> str:
            elem = self.find_element(*self._selected_value)
            return elem.text

    class TripTravelers(Region):
        _root_locator = ('xpath', '(//*[@class="zcIg"]//*[@role="button"])[2]')
        _selected_value = ('css', 'span:nth-child(1)')
        _expand_control = ('css', 'span:nth-child(2)')

        @property
        def selected_value(self) -> str:
            elem = self.find_element(*self._selected_value)
            return elem.text

        @property
        def expand_control(self) -> WebDriverElement:
            return self.find_element(*self._expand_control)

    class TripTravelersDropDown(Region):
        _root_locator = ('css', '.UKFa-dropdownOptions')

        @property
        def loaded(self) -> bool:
            elem = WebDriverWait(self.driver.driver, 10).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, self._root_locator[1]))
            )
            return False if not elem else True

        @property
        def adults(self) -> AdultsOption:
            return self.AdultsOption(self)

        @property
        def child(self) -> ChildOption:
            return self.ChildOption(self)

        class TravelerOption(Region):
            _less = ('css', 'button:nth-of-type(1)')
            _more = ('css', 'button:nth-of-type(2)')
            _input = ('css', 'input')

            @property
            def less(self) -> WebDriverElement:
                return self.find_element(*self._less)

            @property
            def more(self) -> WebDriverElement:
                return self.find_element(*self._more)

            @property
            def input(self) -> int:
                return int(self.find_element(*self._input).value)

        class AdultsOption(TravelerOption):
            _root_locator = ('css', '.u9Xa:nth-of-type(1)')

        class ChildOption(TravelerOption):
            _root_locator = ('css', '.u9Xa:nth-of-type(4)')
            pass

    class DateControl(Region):
        _date_value = ('css', '.cQtq-value')
        _increment = ('css', '.c_E2U-increment')
        _decrement = ('css', 'c_E2U-decrement')

        @property
        def date_value(self) -> date:
            date_value: str = self.find_element(*self._date_value).value
            if date_value:
                # TODO: implement custom parser
                separator_pos = date_value.find('/')
                month = int(date_value[separator_pos + 1:])
                day = int(date_value[separator_pos - 2: separator_pos])
                year = date.today().year  # TODO: support scenarios for dates next year dates
                return date(year, month, day)

        @property
        def increment(self) -> WebDriverElement:
            return self.find_element(*self._increment)

        @property
        def decrement(self) -> WebDriverElement:
            return self.find_element(*self._decrement)

        def increment_days(self, target_date: date):
            delta = target_date - self.date_value
            increment = self.increment
            for i in range(delta.days):
                increment.click()
                sleep(0.25)
