from time import sleep
from datetime import datetime
from behave import given, when, then
from pom import HomePage


@given('The traveler is on the {page} page')
def step_impl(context, page):
    if page == 'Home':
        current_page = context.home_page.open()
        assert current_page.loaded
    else:  # invalid page
        raise ValueError()


@given('The page selected the user origin location by default based on the user location')
def step_impl(context):
    home: HomePage = context.home_page
    assert len(home.origins.list_items) == 1


@given('The page selects trip type "{trip_type}" by default')
def step_impl(context, trip_type: str):
    home: HomePage = context.home_page
    selected_trip_type = home.trip_type
    assert selected_trip_type is not None
    assert selected_trip_type.selected_value == trip_type


@given('The page selects trip class "{trip_class}" by default')
def step_impl(context, trip_class):
    home: HomePage = context.home_page
    selected_trip_class = home.trip_class
    assert selected_trip_class is not None
    assert selected_trip_class.selected_value == trip_class


@given('The page selected a default departure date {days} days in the future')
def step_impl(context, days):
    pass


@given('The page selected a default return date of {days} days in the future')
def step_impl(context, days):
    pass


@given('There are Airports in the desired Origin {origin}')
def step_impl(context, origin):
    pass


@given('There are trips available from {origin_airport_code} to {destiny_airport_code} in Kayak database')
def step_impl(context, origin_airport_code, destiny_airport_code):
    pass


@when("I clean origin's default value")
def step_impl(context):
    home: HomePage = context.home_page
    home.origins.list_items[0].remover.click()


@when('I search for Origin {origin_location_name} and select {origin_airport_code}')
def step_impl(context, origin_airport_code, origin_location_name):
    home: HomePage = context.home_page
    location_dialog = home.location_dialog
    location_dialog.text_box.click()
    location_dialog.text_box.type(origin_location_name)
    result = location_dialog.find_search_result_by_airport_code(origin_airport_code)
    assert result.airport_code.value == origin_airport_code
    result.checkbox.check()
    selected_locations = home.origins.list_items
    assert len(selected_locations) > 0 and f"({origin_airport_code})" in selected_locations[0].selected_value.value


@when(u'I search for Destination {destination_location_name} and selects {destination_airport_code}')
def step_impl(context, destination_airport_code, destination_location_name):
    home: HomePage = context.home_page
    home.destinations.text_box.click()
    # when the dialog lose focus it enters staleness
    home.location_dialog.wait_stale()
    # TODO: retry on selenium.common.exceptions.ElementClickInterceptedException
    sleep(0.25)
    home.destinations.text_box.click()
    sleep(0.25)
    assert home.location_dialog.loaded
    location_dialog = home.location_dialog
    location_dialog.text_box.click()
    location_dialog.text_box.type(destination_location_name)
    result = location_dialog.find_search_result_by_airport_code(destination_airport_code)
    assert result.airport_code.value == destination_airport_code
    result.checkbox.check()
    selected_locations = home.destinations.list_items
    assert len(selected_locations) > 0 and f"({destination_airport_code})" in selected_locations[0].selected_value.value


@when('I select {adults:n} and {child:n} travelers')
def step_impl(context, adults: int, child: int):
    home: HomePage = context.home_page
    home.trip_travelers.expand_control.click()
    assert home.trip_travelers_dropdown.loaded
    trip_travelers_dropdown = home.trip_travelers_dropdown
    add_more_adults = trip_travelers_dropdown.adults.more
    for i in range(adults - 1):  # subtracting the default adult
        add_more_adults.click()
    add_more_child = trip_travelers_dropdown.child.more
    for i in range(child):
        add_more_child.click()
    assert trip_travelers_dropdown.adults.input == adults
    assert trip_travelers_dropdown.child.input == child
    home.title.click()  # Closing the dropdown


@when('I select a new departure {departure_date:ti} and Return {return_date:ti} dates')
def step_impl(context, departure_date: datetime, return_date: datetime):
    # TODO: localize dates
    departure_date = departure_date.date()
    return_date = return_date.date()
    home: HomePage = context.home_page
    home.departing_date_control.increment_days(departure_date)
    home.returning_date_control.increment_days(return_date)
    assert home.departing_date_control.date_value == departure_date
    assert home.returning_date_control.date_value == return_date


@when('I start my search')
def step_impl(context):
    home: HomePage = context.home_page
    home.search_button.click()
    sleep(10)  # TODO: assert on whether the action has properly trigger


@then('The system should display at least one trip from {origin_airport_code} to {destiny_airport_code}'
      'in {departure_date}')
def step_impl(context, origin_airport_code, destiny_airport_code, departure_date):
    pass
